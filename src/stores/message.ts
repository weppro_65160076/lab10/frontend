import { ref, computed } from 'vue'
import { defineStore, mapGetters } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const snackbar = ref(false)
  const text = ref('')
  const showMessage = function (msg: string) {
    text.value = msg
    snackbar.value = true
  }

  return { showMessage, snackbar, text }
})
